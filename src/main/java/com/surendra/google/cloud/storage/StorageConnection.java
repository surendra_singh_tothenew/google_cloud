/**
 * 
 */
package com.surendra.google.cloud.storage;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import com.surendra.google.cloud.util.CloudProperties;

/**
 * @author surendra.singh
 *
 */
public class StorageConnection {

	/**
	 * To connect from App engine running code
	 * 
	 * @param args
	 * @throws Exception
	 */
	public static Storage connectUsingDefaultAccount() {
		return StorageOptions.getDefaultInstance().getService();
	}

	/**
	 * Connect Using a service account private key.
	 * 
	 * IAM -> Service Account -> 
	 * 		Select App Engine Default Service Account -> Click on three dots at right hand side -> Click Create Key
	 *  
	 * @return
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static Storage connectToProjectUsingAccountCredentials() throws FileNotFoundException, IOException {
		return StorageOptions.newBuilder().setProjectId(CloudProperties.getProjectId())
				.setCredentials(GoogleCredentials.fromStream(new FileInputStream(CloudProperties.getAccountPrivateKey())))
				.build().getService();
	}
}
