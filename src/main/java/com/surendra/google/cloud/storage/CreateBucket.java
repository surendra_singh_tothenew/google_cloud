/**
 * 
 */
package com.surendra.google.cloud.storage;

import java.io.FileNotFoundException;
import java.io.IOException;

import com.google.cloud.storage.Bucket;
import com.google.cloud.storage.BucketInfo;
import com.google.cloud.storage.Storage;
import com.surendra.google.cloud.util.Constants;

/**
 * @author surendra.singh
 *
 */
public class CreateBucket {

	/**
	 * @param args
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */
	public static void main(String[] args) throws FileNotFoundException, IOException {
		Storage storage = StorageConnection.connectToProjectUsingAccountCredentials();

		Bucket bucket = storage.create(BucketInfo.of(Constants.BUCKET_NAME));
	    System.out.printf("Bucket %s created.%n", bucket.getName());
	}

}
