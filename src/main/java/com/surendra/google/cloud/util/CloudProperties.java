/**
 * 
 */
package com.surendra.google.cloud.util;

/**
 * @author surendra.singh
 *
 */
public class CloudProperties {

	public static String getProjectId() {
		return System.getenv("GOOGLE_CLOUD_PROJECT");
	}

	public static String getAccountPrivateKey() {
		return System.getenv("GOOGLE_CLOUD_PRIVATE_KEY");
	}
}
